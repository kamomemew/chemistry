module common_variables
 !==共通変数定義==!
    real,allocatable :: dat(:,:)
    real,allocatable :: p(:)
    integer  n   
    character(30),parameter :: path_to_csv= "./data.csv"
    real sig_xy
    real sig_x
    real sig_y
    real sig_sqx
    real sig_sqy
    real mean_x
    real mean_y
    real r
    real a
    real b
    real s
end module

subroutine readCSV
    use common_variables
    implicit none 
    integer :: i=1
    integer x,y
    n=1
    open (3, file=path_to_csv, status='old')
    !行数を調べる
    read(3, '()')
    do
        read (3, *, end=100) !ファイル終端か調べる
        n = n + 1
    end do
    100 continue
    allocate (dat(1:n,2))
    allocate (p(1:n))
    rewind (3) ! ファイルの最初に戻る
    ! 読み込み、配列を返す
    ! 形式は(x,y)
    do i = 1, n
        read (3,'(I4,I4)') x,y
        dat(i,:) = (/x,y/)
        p(i)=x*y
    end do
    close (3)
end subroutine readCSV


subroutine calc_common_variable
    use common_variables ,&
    only:sig_sqx,sig_sqy,sig_x,sig_xy,sig_y,mean_x,mean_y,n,dat
    implicit none
    integer i
    do i=1 ,n
        sig_xy = sig_xy + dat(i,1)*dat(i,2)
        sig_x = sig_x + dat(i,1)
        sig_y = sig_y + dat(i,2)
        sig_sqx = sig_sqx + dat(i,1)**2
        sig_sqy = sig_sqy + dat(i,2)**2
    end do
    mean_x = sig_x / n
    mean_y = sig_y / n
end subroutine calc_common_variable

subroutine calc_regression
    use common_variables ,&
    only:sig_sqx,sig_sqy,sig_x,sig_xy,sig_y,n,r
    implicit none
    r=(sig_xy - sig_x * sig_y/n) / sqrt((sig_sqx - sig_x**2 / n) * (sig_sqy - sig_y**2 / n))
end subroutine calc_regression
 
subroutine calc_least_squares
    use common_variables ,&
    only:mean_x,mean_y,sig_x,sig_sqx,sig_xy,b,a
    implicit none
    b=(sig_xy - sig_x * mean_y) / (sig_sqx - sig_x * mean_x)
    a = mean_y - b * mean_x
end subroutine calc_least_squares

subroutine calc_standard_deviation
    use common_variables ,&
    only:n,sig_sqy,mean_y,s,r
    implicit none
    s=sqrt((sig_sqy - 2 * mean_y**2)*(1-r**2) / (n-2))
end subroutine calc_standard_deviation

subroutine sort
    use common_variables,&
    only:n,p
    implicit none
    integer i ,j
    integer :: min_in_loop
    real tmp
    do i=1,n
    min_in_loop=i
        do j=i,n
            if (p(min_in_loop) >=  p(j)) then
                min_in_loop = j
            end if    
        end do
        tmp = p(i)
        p(i) = p(min_in_loop)
        p(min_in_loop)=tmp

    end do
end subroutine sort

program main
    use common_variables
    implicit none
    integer f
    call readCSV
    call calc_common_variable
    call calc_regression
    call calc_least_squares
    call calc_standard_deviation
    call sort
    print '("r=",f14.5)',r
    print '("a=",f14.5)',a
    print '("b=",f14.5)',b    
    print '("s=",f14.5/)',s
    print '("! ==ソート済みのデータを表示します== !" )'
    do f=1,n
        print *,p(f)
    end do
end program main 
