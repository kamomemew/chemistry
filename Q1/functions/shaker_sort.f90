program shaker_sort
    implicit none
    integer,parameter::n=2000
    integer ::i
    integer ::dat(0:n-1)
    integer ::top_index =0
    integer ::bot_index =n-1
    integer ::swap
    integer ::last_swap_index
    
    !=== 値をセット ===!
    do i=0 ,n-1
        read*,dat(i)
    end do
    !=== ソート ===!
    do
        last_swap_index = top_index
        do i=top_index, bot_index-1
            if (dat(i) > dat(i+1)) then
                swap=dat(i)
                dat(i)=dat(i+1)
                dat(i+1)=swap
                last_swap_index = i
            end if
        end do
        bot_index = last_swap_index 
        if (top_index == bot_index) then
            exit
        end if
        last_swap_index = bot_index
        do i = bot_index, top_index+1, -1
        
            if (dat(i) < dat(i-1)) then
                swap=dat(i)
                dat(i)=dat(i-1)
                dat(i-1)=swap
                last_swap_index = i
            end if
        end do
        top_index = last_swap_index
        if (top_index == bot_index) then
            exit
        end if
    end do    
    !=== 結果表示 ===!
    do i=0 ,n-1
        print*,dat(i)
    end do
end program shaker_sort
