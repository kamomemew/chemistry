program norm_sort ! ノームソートで実装
    integer,parameter :: n = 2000
    integer ::i=n,j
    integer ::dat(n)
    integer ::swap
    !=== 値をセット ===!
    do j=1 ,n
        read*,dat(j)
    end do
    !=== ソート ===!
    do
        if (i == 0) then
            exit
        end if
        if (dat(i) >= dat(i-1) ) then
            i=i-1
        else
            swap=dat(i)
            dat(i)=dat(i-1)
            dat(i-1)=swap
            i=i+1
            if (i>=n+1) then 
                i=i-1
            end if
        end if
    end do
    !=== 結果表示 ===!
    do i=n,1,-1
    print *,dat(i)
    end do
end program norm_sort
