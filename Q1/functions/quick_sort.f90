program quick_sort
implicit none
integer,parameter :: n = 2000
integer ::dat(0:n-1),t
    !=== 値をセット ===!
    do t=0 ,n-1
        read*,dat(t)
    end do
    call quicksort(0,n)
    do t=0 ,n
        print *,dat(t)
    end do    
contains

function med3(x, y, z) !中間値を与える
    implicit none
    integer ::x,y,z,med3
    if (x < y) then
        if (y < z) then 
            med3 = y 
        else 
            if (z < x) then
                med3 = x 
            else 
                med3 = z
            end if
        end if 
    else
        if (z < y)then
            med3 = y 
        else 
            if (x < z) then
                med3 = x 
            else 
                med3 = z
            end if
        end if
    end if
end function med3

recursive subroutine quicksort(left,right) 
    implicit none
    integer:: left,right,i,j,swap,pivot
    ! left  : ソートするデータの開始位置
    ! right : ソートするデータの終了位置

    if (left < right) then
        i = left
        j = right
        pivot = med3(dat(i), dat(i + (j - i)/ 2), dat(j))!* (i+j)/2ではオーバーフローしてしまう */
        do 
            do while (dat(i) < pivot) 
            i=i+1     !* dat(i) >= pivot となる位置を検索 */
            end do
            do while (pivot < dat(j)) 
            j=j-1 !* dat(j) <= pivot となる位置を検索 */
            end do
            if (i >= j) then 
                exit
            end if
            swap=dat(i)
            dat(i)=dat(j)
            dat(j)=swap
            i=i+1 
            j=j-1
        end do
        call quicksort(left, i - 1)  !/* 分割した左を再帰的にソート */
        call quicksort(j + 1, right) !/* 分割した右を再帰的にソート */
    end if
end subroutine quicksort
end program quick_sort
