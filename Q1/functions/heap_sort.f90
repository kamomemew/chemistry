program heap_sort
    implicit none
    integer,parameter :: n = 2000
    integer ::dat(n)
    integer::j
    !=== 値をセット ===!
    do j=0 ,n-1
        read*,dat(j)
    end do
    call heapsort(n)
    do j=0 ,n-1
        print *,dat(j)
    end do    
    contains


subroutine swap(a,b)
    implicit none 
    integer::a,b,tmp
    tmp = dat(a)
    dat(a) = dat(b)
    dat(b) = tmp
end subroutine swap

subroutine heapsort(n_elems)
    implicit none
    integer ::n_elems
    integer::i,l
    do i=0,n_elems
        call upheap(i)
    end do
    do l=i-1,1,-1
        call swap(0,l)
        call downheap(l)
    end do
end subroutine heapsort

subroutine upheap(k)
    implicit none
    integer m,k
    do while (k > 0) 
        m =  (((k) + 1) / 2 - 1)
        if (dat(m) < dat(k))then 
            call swap(m,k)
        else 
            exit
        end if
        k = m
    end do
end subroutine upheap

    subroutine downheap(k)
            implicit none
        integer::k,m,tmp ,l_chld,r_chld
        m = 0
        tmp = 0

        do 
            l_chld =  ((m + 1) * 2 - 1)
            r_chld = ((m + 1) * 2)
    
            if (l_chld >= k) then
                exit
            end if

            if (dat(l_chld) > dat(tmp)) then
                tmp = l_chld
            end if
            if ((r_chld < k) .AND. (dat(r_chld) > dat(tmp))) then
                tmp = r_chld
            end if
            if (tmp == m) then
                exit
            end if
            call swap(tmp, m)
            m = tmp
        end do
    end subroutine downheap
end program heap_sort
