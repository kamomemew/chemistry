program comb_sort
    integer,parameter :: n = 2000
    integer ::i=n,j
    integer ::dat(n)
    integer ::swap
    integer ::h = n
    logical ::is_swapped = .false.
    !=== 値をセット ===!
    do j=1 ,n
        read*,dat(j)
    end do
    

    do while (h > 1 .or. is_swapped) 
        if (h > 1) then
            h = (h * 10) / 13
        end if

        is_swapped = .false.
        do  i = 0,n-h-1  
            if (dat(i) > dat(i+h)) then
                swap = dat(i)
                dat(i) = dat(i+h)
                dat(i+h) = swap
                is_swapped = .true.
            end if
        end do
    end do
    
    !=== 結果表示 ===!
    do i=n,1,-1
    print *,dat(i)
    end do
end program
