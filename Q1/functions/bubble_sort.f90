program bubble
    implicit none
    integer,parameter ::n = 2000 !データ数
    integer ::i,k,j
    integer ::sort_is_over=0
    integer ::dat(n)
    integer ::swap
    !=== 値をセット ===!
    do k=1 ,n
        read*,dat(k)
    end do
    !=== ソート ===!
    do i=1 ,n-1
        do j=1,n-1
            if (dat(j)<dat(j+1)) then
                swap=dat(j)
                dat(j)=dat(j+1)
                dat(j+1)=swap
            else
                sort_is_over=sort_is_over+1
            end if
        end do
        if (sort_is_over == n-1) then
            exit
        else 
        sort_is_over = 0
        end if
    end do
    !=== 結果表示 ===!
    do k=1 ,n
        print *,dat(k)
    end do
end program bubble
