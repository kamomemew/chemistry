program Newton_Raphson
    implicit none
    double precision,parameter::e=2.71828182845904524d0
    double precision,parameter::pi=3.14159265358979324d0
    double precision ::x_0,x_n
    double precision Delta_min,Epsilon
    
    print*,"開始位置を入力："
    read*,x_0
    x_n=x_0 
    print*,"停止条件を入力:Delta"
    read *,Delta_min
    print*,"停止条件を入力:Epsilon"    
    read *,Epsilon
    
    do
        if (F(x_n)< Delta_min) then
            exit
        end if
        if (abs(x_n-x_n-F(x_n)/DF(x_n))<Epsilon) then
            exit
        end if
        x_n = x_n-F(x_n)/DF(x_n)
    end do
    print *,"近似解は"
    print *,x_n
contains
    function DF(x)
        implicit none
        double precision DF,x
        double precision,parameter::dif=0.000000000001d0
        DF=(F(x+dif)-F(x))/dif
    end function DF

    function F(x)
        implicit none
            double precision F,x
            F=x**3+6*x**2+21*x+32
            !F=x-e**(-x)
            !F=tan(x)-(1/x)
    end function F

end program Newton_Raphson
