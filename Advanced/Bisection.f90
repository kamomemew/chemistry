program Bisection_Method
    implicit none
    double precision x_0,x_1,x_2,center
    double precision Delta
    double precision Delta_min

    print*,"開始位置を入力："
    read*,x_0 
    print*,"停止条件を入力:"
    read *,Delta_min
    Delta=1d0     
    
    !!=== 区間の設定 ===!!
    do
        if (F(x_0+Delta)*F(x_0-Delta)<=0) then
            x_1=x_0-Delta
            x_2=x_0+Delta
            exit
        else
            Delta=Delta*2
        end if 
    end do
    
    !!=== 収束させる ===!!
    do
        center=(x_1+x_2)/2
        if(F(x_0)*F(center)<0)then
            x_1=center
        else
            x_2=center 
        end if
        
        if(abs(x_1-x_2)<Delta_min)then
            exit
        end if
        
        if(abs(F(center)) < 10**(-7))then
            print*,F(center)
            exit
        end if
    end do
    
    print *,"近似解は、"
    print *,center
    
contains
    function F(x)
        implicit none
        double precision F
        double precision x
        F=x**3+6*x+21*x+32
    end function F
end program Bisection_Method
