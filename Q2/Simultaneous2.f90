program Linier
    implicit none
    ! ===変数定義===
    integer :: d        !(行列の次数)
    integer :: mat_m    !(行列の行)
    integer :: mat_n    !(行列の列)
    integer :: piv      !(ピボット番号)
    integer :: proc     !(処理中)
    integer :: ex_m     !(交換対象の行)
    integer :: ex_n     !(交換対象の列)
    integer :: r        !(結果表示用)
    integer :: s        !(順序を戻す)
    real,allocatable :: swap_m(:)
    real,allocatable :: swap_n(:)
    real :: swap_ans
    real :: max_cache
    real,allocatable :: linior(:,:)
    integer,allocatable :: seq(:)      !(列交換の順序)
            
    ! ===数式取得、2次元配列に格納===
    print*,"次元は？"
    read *,d
    allocate (linior(d,d+1))  ! 確保すべき量が確定
    allocate (swap_m(d+1))
    allocate (swap_n(d))      ! 行交換時のスワップ
    allocate (seq(d))
    do mat_m=1,d
      do mat_n=1,d
        print '("式",I2,"のx_",I2,"?")',mat_m,mat_n
        read *,linior(mat_m,mat_n)
      end do
        print '("式",I2,"の右辺","?")',mat_m
        read *,linior(mat_m,d+1)
    end do
    
    do piv=1 ,d
    max_cache = maxval(abs(linior(piv:d,piv:d)))
        do ex_m=piv,d
            do ex_n=piv,d
                if(abs(linior(ex_m,ex_n))==max_cache) then
                    !===行(m)の交換===!
                    swap_m(:)=linior(piv,:)
                    linior(piv,:)=linior(ex_m,:)
                    linior(ex_m,:)=swap_m(:)
                    !===列(n)の交換===!
                    swap_n(:)=linior(:,piv)
                    linior(:,piv)=linior(:,ex_n)
                    linior(:,ex_n)=swap_n
                    seq(piv)=ex_n
                    goto 100 !なくてもいいがあると速い
                end if
            end do
        end do
        100 continue
        do proc =1,d
            if(proc/=piv)then
                linior(proc,:)=linior(proc,:)-(linior(proc,piv)/linior(piv,piv))*linior(piv,:)
            end if
        end do
    end do



    ! ===拡大行列係数部を linior(i,i)成分で割り、順に答えを得る===
    do r=1 ,d
      linior(r,d+1)=linior(r,d+1)/linior(r,r)
    end do
    ! ===順序を戻す=== !
    do s=d,1,-1
        swap_ans=linior(seq(s),d+1)
        linior(seq(s),d+1)=linior(s,d+1)
        linior(s,d+1)=swap_ans
    end do
    ! ===答えの表示=== !
    do r=1,d
        print *,linior(r,d+1)
    end do

end program Linier

