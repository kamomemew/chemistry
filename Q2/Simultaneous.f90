program Linier
    implicit none
    ! ===変数定義===
    integer :: d        !(行列の次数)
    integer :: mat_row  !(行列の↓row)
    integer :: mat_line !(行列の→line)
    integer :: piv      !(ピボット対象の行)
    integer :: proced !(処理済みの行)
    integer :: ex       !(交換対象の行)
    integer :: r        !(結果表示用)
    real,allocatable :: swap(:)
    real,allocatable :: linior(:,:)
        
    ! ===数式取得、2次元配列に格納===
    print*,"次元は？"
    read *,d
    allocate (linior(d,d+1))  ! 確保すべき量が確定
    allocate (swap(d+1))      ! 行交換時のスワップ
    do mat_row=1,d
      do mat_line=1,d
        print '("式",I2,"のx_",I2,"?")',mat_row,mat_line
        read *,linior(mat_row,mat_line)
      end do
        print '("式",I2,"の右辺","?")',mat_row
        read *,linior(mat_row,d+1)
    end do
    
    ! ===↓↓↓下向きに消去していく（前進消去）↓↓↓===
    do piv=1,d !l=ピボット対象の行、はじめの行から
        do ex=piv+1,d
            if (linior(ex,piv)==maxval(linior(:,piv))) then 
               swap(:)=linior(ex,:)
               linior(ex,:)=linior(piv,:)
               linior(piv,:)=swap(:)
            end if 
        end do
        ! 自身より上の行は処理済み
        ! 係数は linior(proced,i)/linior(proced,proced)
        do proced=1 ,piv-1
            linior(piv,:)=linior(piv,:)-(linior(piv,proced)/linior(proced,proced))*linior(proced,:)
        end do   
    end do
    
    ! ===配列は下三角行列になった===
    ! ===↑↑↑今度は下から消去(後進代入)↑↑↑===
    do piv=d,1,-1!対象の行、最終行から
        do proced=d ,piv+1, -1 !自身より下の行で消去
            linior(piv,:)=linior(piv,:)-(linior(piv,proced)/linior(proced,proced))*linior(proced,:)
            print *,proced
        end do
    end do
    ! ===配列は対角行列になった===
    
    ! ===拡大行列係数部を linior(i,i)成分で割り、順に答えを得る===
    do r=1 ,d
      linior(r,d+1)=linior(r,d+1)/linior(r,r)
      print *,linior(r,d+1)
    end do

end program Linier
